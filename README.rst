dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install

Patches
-------

In this repo the following patches are available (with new or updated
key bindings shown):

- `attachdirection <https://dwm.suckless.org/patches/attachdirection/>`_
- `bottomstack <https://dwm.suckless.org/patches/bottomstack/>`_
    - ``Alt+U``: bottomstack layout
    - ``Alt+O``: horizontal bottomstack layout
- `colorbar <https://dwm.suckless.org/patches/colorbar/>`_
- `columns <https://dwm.suckless.org/patches/columns/>`_
    - ``Alt+C``: columns layout
- `combo <https://dwm.suckless.org/patches/combo/>`_
    - ``Alt+[multiple numbers]``: select multiple tags at once
- `cursorwarp <https://dwm.suckless.org/patches/cursorwarp/>`_
- `deck <https://dwm.suckless.org/patches/deck/>`_
    - ``Alt+E``: deck layout
- `decorhints <https://dwm.suckless.org/patches/decorhints/>`_
- `fakefullscreen <https://dwm.suckless.org/patches/fakefullscreen/>`_
- `fibonacci <https://dwm.suckless.org/patches/fibonacci/>`_
    - ``Alt+R``: spiral layout
    - ``Alt+Shift+R``: dwindle layout
- `flextile <https://dwm.suckless.org/patches/flextile/>`_
- `fullscreen <https://dwm.suckless.org/patches/fullscreen/>`_
    - ``Alt+Shift+F``: toggle client to fullscreen
- `gridmode <https://dwm.suckless.org/patches/gridmode/>`_
    - ``Alt+G``: grid layout
- `inplacerotate <https://dwm.suckless.org/patches/inplacerotate/>`_
    - ``Alt+Shift+J``: rotate stack or master counter clockwise
    - ``Alt+Shift+K``: rotate stack or master clockwise
    - ``Alt+Shift+H``: rotate everything counter clockwise
    - ``Alt+Shift+L``: rotate everything clockwise
- `onlyquitonempty <https://dwm.suckless.org/patches/onlyquitonempty/>`_
    - ``Alt+Shift+Q``: quit if no client is open
    - ``Alt+Shift+Ctrl+Q``: force quit
- `pango <https://dwm.suckless.org/patches/pango/>`_
- `rainbowtags <https://dwm.suckless.org/patches/rainbowtags/>`_
- `rmaster <https://dwm.suckless.org/patches/rmaster/>`_
    - ``Alt+Shift+M``: swap master between left and right
- `sticky <https://dwm.suckless.org/patches/sticky/>`_
    - ``Alt+S``: make a client sticky (visible on all tags)
- `tatami <https://dwm.suckless.org/patches/tatami/>`_
    - ``Alt+Y``: tatami layout

The branch ``upstream`` is identical to the official dwm master
branch. The ``master`` branch is the version I use currently,
with most of the patches merged, and a few config values
customized. Each patch is available on its own branch, these
are all rebased to upstream, which means you can merge one
after each with relatively few merge conflicts. After each
merge it's recommended to recompile and test. Some patches
might be incompatible between each other. For example the
``colorbar`` patch breaks the status bar, and ``rmaster``
does not seem to work with ``flextile``. Another important
point: ``pango`` does not support bitmap fonts any more.

Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
